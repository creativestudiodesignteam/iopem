$( document ).ready(function() {
    $('.leftmenutrigger').on('click', function(e) {
    $('.side-nav').toggleClass("open");
    e.preventDefault();
   });
});

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".navbar").addClass("bg-dynamic-menu");
        $(".-icon-mobile").addClass("color-dynamic-menu");
    } else {
        $(".navbar").removeClass("bg-dynamic-menu");
        $(".-icon-mobile").removeClass("color-dynamic-menu");
    }
});

var $doc = $('html, body');
$('a').click(function() {
    $doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});

/* $(function () {
    $('[data-toggle="tooltip"').tooltip({ trigger: "click" });
});
 */
$(function() {
    var HasTooltip = $('.dot');
    HasTooltip.on('click', function(e) {
      e.preventDefault();
      var isShowing = $(this).data('isShowing');
      HasTooltip.removeData('isShowing');
      if (isShowing !== 'true')
      {
        HasTooltip.not(this).tooltip('hide');
        $(this).data('isShowing', "true");
        $(this).tooltip('show');
      }
      else
      {
        $(this).tooltip('hide');
      }
  
    }).tooltip({
      animation: true,
      trigger: 'manual'
    });
  });
/* 
  $(function () {
 
    $('.tooltip-demo').tooltip()
   
  }) */
   

/*Form contact*/
//Contact Us
function crud(formulario){

    var postData = $(formulario).serializeArray();
    var formURL = $(formulario).attr("action");
    
    $.ajax({
            type: "POST",
            url: formURL,
            data: postData,
            dataType: 'json',
            success: function(data){

                var msg = data['mensagem'];
            var classe = data['classe'];

            $("#simple-msg").html('<div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 '+classe+'"><b>'+msg+'</b></div>');
            $("#simple-msg").slideDown();
            setTimeout(function(){$("#simple-msg").slideUp();},4000);

            if(data['result'] === 'error'){
                return false;
            }else if(data['result'] === 'success'){
                $(formulario)[0].reset();
            }

            }
    });
}
