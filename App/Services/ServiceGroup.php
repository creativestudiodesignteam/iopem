<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\AccessGroup;


class ServiceGroup{

    public function create($request) {
        if($request){
            $obj = new AccessGroup();
            $this->save($request, $obj);

            $return = [];
            if($obj->idgroup <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o grupo <strong>{$obj->description}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = AccessGroup::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idgroup <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o grupo <strong>{$obj->description}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $grupo = AccessGroup::find($id);
        $grupo->status = 'd';
        $resp = $grupo->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->description = $request['title'];
        $obj->status = $request['status'];

        $obj->save();

        if(isset($request['menus']) && count($request['menus']) > 0){
            $obj->menus()->sync($request['menus']);
        }

    }

}
