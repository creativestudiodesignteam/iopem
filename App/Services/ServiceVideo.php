<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Video;


class ServiceVideo
{

    public function create($request)
    {
        if ($request) {
            $obj = new Video();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idvideo <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a Video <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = Video::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idvideo <> '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a Video <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = Video::find($id);
        $obj->status = 'd';
        $resp = $obj->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $obj->title = $request['title'];

        if(isset($request['date_create'])){
            $obj->date_create = $request['date_create'];
        }
        
        $obj->date_update = $request['date_update'];
        $obj->posting_date = $request['posting_date'];
        $obj->status = $request['status'];
        $obj->iduser = $request['iduser'];
        $obj->url = $request['url'];

        $obj->save();
    }
}
