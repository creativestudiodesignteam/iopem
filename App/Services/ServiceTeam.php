<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Team;


class ServiceTeam{

    public function create($request) {
        if($request){
            $obj = new Team();
            $this->save($request, $obj);

            $return = [];
            if($obj->idteam <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = Team::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idteam <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $departamento = Team::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->title = $request['title'];
        $obj->text = $request['text'];
        $obj->status = $request['status'];
        $obj->image = $request['image'];
        $obj->office = $request['office'];

        $obj->save();


    }

}
