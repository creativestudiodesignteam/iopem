<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteSeo;


class ServiceSeo{

    public function create($request) {
        if($request){
            $obj = new SiteSeo();
            $this->save($request, $obj);

            $return = [];
            if($obj->idseo <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o <strong>Registro</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteSeo::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idseo <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o <strong>Registro</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $departamento = SiteSeo::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->keywords = $request['keywords'];
        $obj->description = $request['description'];

        $obj->save();

    }

}
