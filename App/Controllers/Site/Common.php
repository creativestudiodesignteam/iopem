<?php

namespace App\Controllers\Site;


use \App\Controllers\Site\Config;
use \App\Models\Entities\Configuration;
use \App\Models\Entities\SiteSeo;
use App\Models\Entities\SiteBanner as AppSiteBanner;



/**
 * Description of Common
 *
 * @author oseas
 */
class Common extends Config{

    public function __construct(){
        parent::__construct();
        $this->datasite = $this->info();
        $this->banners = AppSiteBanner::where('status', '=', 'a')->get();

    }

    public function info(){
        $info = [];
        $info['config'] = Configuration::find(1);
        $info['seo'] = SiteSeo::find(1);

        return $info;
    }

}
