<?php

namespace App\Controllers\Site;

use App\Classes\Mail;
use \App\Controllers\Site\Common;
use App\Models\Entities\Blog;
use App\Models\Entities\Contacts;
use App\Models\Entities\SiteInstitucional;
use App\Models\Entities\Team;
use App\Models\Entities\Unit;

/**
 * Description of Home
 *
 * @author oseas¹
 */
class Home extends Common{

    protected $pagelink = '';

    public function index() {
        $this->start_session();
        $this->view->page = 'header-home';
        $this->view->titlehead = 'Home';
        $this->view->about = SiteInstitucional::where('ativo', '=', 'a')->first();
        $this->view->team = Team::where('status', '=', 'a')->get();
        $units = Unit::where('status', '=', 'a')->orderBy('order', 'ASC')->get();
        $this->view->blogs = Blog::where('status', '=', 'a')
                                    ->where('posting_date', '<=', date('Y-m-d'))
                                    ->orderBy('posting_date', 'DESC')
                                    ->limit(3)
                                    ->get();

        $this->view->units = [];

        foreach($units as $u){
            $this->view->units[] = $u;
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function formunits(){
        $this->render('form-unidades', $this->folder, $this->page, false);
    }

    public function contact(){
        if($_POST){
            $unit = Unit::find($_POST['destinatario']);

            $contact = new Contacts();
            $contact->name = $_POST['name'];
            $contact->email = $_POST['email'];
            $contact->idunits = $unit->idunits;
            $contact->phone = isset($_POST['phone']) ? $_POST['phone'] : '';
            $contact->subject = isset($_POST['subject']) ? $_POST['subject'] : '';
            $contact->text = $_POST['message'];
            $contact->date_create = date('Y-m-d H:i:s');
            $contact->view = 'n';

            //$email[] = array('name' => $unit->locate, 'email' => $unit->email);
            $email[] = array('name' => utf8_decode($unit->locate), 'email' => 'contato@oseasmoreto.com');

            $contact->save();

            $mail = new Mail(
                $this->datasite['config']->emailuser, 
                $this->datasite['config']->emailpassword, 
                $this->datasite['config']->smtp, 
                $this->datasite['config']->port, 
                '#454d38',
                utf8_decode($this->datasite['config']->title),
                'https://www.iopem.com.br/assets/images/logo.png'
            );
            $mail->fromname = utf8_decode($unit->locate);
            $mail->recipient = $email;
            $mail->subject = addslashes(utf8_decode($unit->locate) . ' | Nova contato do site!');
            $mail->body = utf8_decode('
                <tr>
                <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse" align="center">
                <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                <b>Dados do Contato:</b><br><br>
                <b>Data: '.$contact->date_create.'</b><br>
                <b>Nome: '.$contact->name.'</b><br>
                <b>E-mail: '.$contact->email.'</b><br>
                <b>Telefone: '.$contact->phone.'</b><br>
                <b>Assunto: '.$contact->subject.'</b><br>
                <b>Mensagem: '.$contact->text.'</b><br>
                </div>
                </td>
                </tr>
                <tr>
                <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse;" align="center">
                <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center; margin-top: 10px">
                </div>
                </td>
                </tr>
            ');

            $mail->altbody = utf8_decode($this->datasite['config']->title.' | Novo contato do site');

            $send = $mail->send();

            if($send){
                $return['response']['mensagem'] = 'Mensagem enviada com sucesso!';
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/home';
            }else{
                $return['response']['mensagem'] = 'Não foi possivel enviar a mensagem!';
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }
    }
}
