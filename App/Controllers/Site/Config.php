<?php

namespace App\Controllers\Site;

use \App\Core\Controller;
use App\Models\Entities\Configuration;

/**
 * Description of Config
 *
 * @author oseas
 */
class Config extends Controller{
    protected $folder = 'Site';
    protected $page = 'layout';
    protected $template = 'Default';
    protected $gkey = '';
    protected $header = '';
    protected $motel_id = 1;
    protected $url_default_template = '/templates/site/default';
    protected $url_default_site = 'http://127.0.0.1:8000';
    protected $atual = 'd';
    protected $titulo_pagina = 'Home';
    protected $pagelink = '';
    protected $datasite;

    public function __construct(){
        $this->view = new \stdClass();
        $this->url_default_site = 'http://'.$_SERVER['HTTP_HOST'].'';
    }



}
