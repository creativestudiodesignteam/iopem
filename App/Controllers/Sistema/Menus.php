<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessMenu;
use App\Services\ServiceMenu;

/**
 * Description of Home
 *
 * @author oseas
 */
class Menus extends Common
{

    protected $service;
    protected $pagelink = 'sistema/menus';
    protected $groups;
    protected $levelzero;
    protected $id_class = 'usuarios';
    protected $obj;

    public function __construct()
    {
        $this->view = new \stdClass();
        $this->service = new ServiceMenu();
        $this->groups = AccessGroup::where('status', '=', 'a')->get();
        $this->levelzero = AccessMenu::where('level', '=', 0)->where('status', '=', 'a')->get();
        $this->obj = new AccessMenu();
        $this->obj->status = 'a';

        $this->view->levelzero = $this->levelzero;
    }

    public function index()
    {
        $this->start_session();

        $this->titulo_pagina = 'Menus';
        $objs = AccessMenu::where('status', '<>', 'd')->get();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Menus';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->url = '/' . $this->pagelink;
        $this->view->titulo = 'Lista de Menus';
        $this->view->objs = $objs;

        foreach ($this->view->objs as $o) {
            $o->levelpreview = $o->level <> 0 ? AccessMenu::find($o->level)->title . '->' : '';
        }

        if (isset($_SESSION['message'])) {
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function create()
    {
        $this->start_session();
        $this->titulo_pagina = 'Menus - Cadastro';
        $this->id_class  = 'cadastrar-anuncio';
        $obj = $this->obj;

        $groups = [];
        foreach ($this->groups as $m) {
            $groups['group'][] = $m;
            $groups['ckecked'][] = $this->checkmenugroup($m->idgroup, $obj->idmenu);
        }

        $this->view->groups = $groups;

        $breadcrumb['url'][0] = '/' . $this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Menus';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#.';
        $breadcrumb['label'][1] = 'Cadastrar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/' . $this->pagelink;
        $this->view->action = '/' . $this->pagelink . '/create';
        $this->view->titulo = 'Lista de Menus';
        $this->view->label = 'Cadastrar';
        $this->view->class_btn = 'btn btn-success';
        $this->view->obj = $this->obj;

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Menu <strong>{$obj->titulo}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Menu <strong>{$obj->titulo}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function update($id)
    {
        $this->start_session();
        $this->titulo_pagina = 'Menus - Editar';
        $this->id_class  = 'cadastrar-anuncio';
        $obj = AccessMenu::find($id);


        $groups = [];
        foreach ($this->groups as $m) {
            $groups['group'][] = $m;
            $groups['ckecked'][] = $this->checkmenugroup($m->idgroup, $obj->idmenu);
        }

        $this->view->groups = $groups;

        $breadcrumb['url'][0] = '/' . $this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Menus';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#.';
        $breadcrumb['label'][1] = 'Alterar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = $this->pagelink;
        $this->view->action = '/' . $this->pagelink . '/update/' . $id;
        $this->view->titulo = 'Lista de Menus';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Menu <strong>{$obj->titulo}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Menu <strong>{$obj->titulo}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function destroy($id)
    {
        $this->start_session();
        $obj = AccessMenu::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Página <strong>{$obj->titulo}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir a página <strong>{$obj->titulo}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
