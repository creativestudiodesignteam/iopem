<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use App\Models\Entities\Contacts;
use App\Models\Entities\Unit;

/**
 * Description of Home
 *
 * @author oseas
 */
class Contact extends Common
{

  protected $pagelink = 'contacts';

  public function __construct()
  {
    $this->start_session();
    $this->logado();
    $this->view = new \stdClass();
  }

  public function index()
  {
    $this->start_session();
    $this->titulo_pagina = 'Contatos';
    $this->logado();
    set_time_limit(600);

    $breadcrumb['url'][0] = '#.';
    $breadcrumb['label'][0] = 'Lista de contatos';
    $breadcrumb['active'][0] = 'active';
    $this->view->breadcrumb = $breadcrumb;

    $this->view->url = '/' . $this->pagelink;
    $this->view->titulo = 'Lista de contatos';

    if(isset($_GET['idunits'])){
      $this->view->contacts = Contacts::where('idunits', '=', $_GET['idunits'])->get();
    }else{
      $this->view->contacts = Contacts::all();
    }
    
    $this->view->units = Unit::where('status', '<>', 'd')->get();

    $this->view->url = '/' . $this->pagelink;
    $this->render('index', $this->folder, $this->page);
  }

  public function view($id){
    $this->start_session();

    $obj = Contacts::find($id);
    $obj->view = 's';
    $obj->save();

    $this->titulo_pagina = 'Contato - Vizualizar';
    $this->id_class = 'cadastrar-anuncio';

    $breadcrumb['url'][0] = '/' . $this->pagelink;
    $breadcrumb['label'][0] = 'Lista de Contatos';
    $breadcrumb['active'][0] = 'active';
    $breadcrumb['url'][1] = '#.';
    $breadcrumb['label'][1] = 'Alterar';
    $breadcrumb['active'][1] = 'active';
    $this->view->breadcrumb = $breadcrumb;

    $this->view->url = $this->pagelink;
    $this->view->action = '/' . $this->pagelink . '/update/' . $id;
    $this->view->titulo = 'Lista de Contatos';
    $this->view->label = 'Responder';
    $this->view->class_btn = 'btn btn-info';
    $this->view->obj = $obj;

    $this->render('view', $this->folder, $this->page);

  }

}
