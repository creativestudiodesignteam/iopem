<?php

namespace App\Controllers\Sistema;

use \App\Core\Controller;


/**
 * Description of Config
 *
 * @author oseas
 */
class Config extends Controller{
    protected $folder = 'Sistema';
    protected $page = 'layout';
    protected $template = 'Default';
    protected $gkey = '';
    protected $header = '';
    protected $url_default_template = '/templates/sistema/default';
    protected $url_default_site = 'http://127.0.0.1:8000/sistema';
    protected $atual = 'd';
    protected $titulo_pagina = 'Dashboard';
    protected $pagelink = '';
    protected $datasite;

    public function __construct(){
        $this->view = new \stdClass();
        $this->url_default_site = 'http://'.$_SERVER['HTTP_HOST'].'/sistema';
    }

}
