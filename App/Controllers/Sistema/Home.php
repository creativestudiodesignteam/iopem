<?php

namespace App\Controllers\Sistema;

use App\Models\Entities\Contacts;
use App\Models\Entities\Unit;
use \App\Controllers\Sistema\Common;
use \App\Models\Entities\AccessUser;
use \App\Services\ServiceAccessUser;

/**
 * Description of Home
 *
 * @author oseas
 */
class Home extends Common
{

    protected $pagelink = 'sistema';

    public function __construct()
    {
        $this->start_session();
        $this->logado();
        $this->view = new \stdClass();
        $this->uservice = new ServiceAccessUser();

        $user = AccessUser::find($_SESSION['user_id']);
        $this->client = $user->client;
    }

    public function index()
    {
        $this->start_session();
        $this->titulo_pagina = 'Dashboard';
        $this->logado();
        set_time_limit(600);

        $breadcrumb['url'][0] = '#.';
        $breadcrumb['label'][0] = 'Dashboard';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->contacts = Contacts::all();
        $this->view->units = Unit::where('status', '<>', 'd')->get();

        $this->view->url = '/' . $this->pagelink;
        $this->render('index', $this->folder, $this->page);
    }

    public function perfil($id)
    {
        $this->start_session();
        $this->titulo_pagina = 'Perfil';
        $obj = AccessUser::find($id);

        $breadcrumb['url'][0] = '#.';
        $breadcrumb['label'][0] = 'Perfil';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/' . $this->pagelink;
        $this->view->action = '/' . $this->pagelink . '/perfil/' . $id;
        $this->view->titulo = 'Perfil';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;

        if ($_POST) {
            $request = $this->uservice->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Perfil <strong>{$obj->name}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink . '/perfil/' . $id;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        $this->render('perfil', $this->folder, $this->page);
    }

    public function uploads()
    {
        $this->start_session();
        if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
            $responseimg = $this->upload($_FILES, 'usuarios');

            if ($responseimg['success']) {
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['image'] = $responseimg['image'];
                $return['response']['status'] = 1;
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
                $return['response']['status'] = 0;
            }

            $return['response']['mensagem'] = $responseimg['message'];
            print_r(json_encode($return));
            exit();
        }
    }

}
