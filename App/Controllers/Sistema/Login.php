<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use App\Models\Entities\AccessUser;
use App\Classes\Mail;

/**
 * Description of Home
 *
 * @author oseas
 */
class Login extends Common
{

    protected $titulo_pagina = 'Login';

    public function index()
    {
        $this->render('index', $this->folder, 'login');
    }

    public function logar($data = [])
    {
        $this->start_session();

        $admin = AccessUser::where('user', '=', $data['username'])->where('password', '=', md5($data['password']))->where('status', '=', 'a')->first();

        if (isset($admin->iduser) && $admin->iduser <> '') {
            $_SESSION['user'] = $admin->user;
            $_SESSION['password'] = $admin->password;
            $_SESSION['idgroup'] = $admin->idgroup;
            $_SESSION['namegroup'] = $admin->group->description;
            $_SESSION['user_id'] = $admin->iduser;
            $_SESSION['name'] = $admin->name;
            $_SESSION['code_nike'] = $admin->group->description;
            $_SESSION['image'] = $admin->image;
            $_SESSION['tipo'] = 'a';
        } else {
            $return['response']['mensagem'] = 'Usuário ou senha incorreto!';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        }

        $return = [];

        $return['response']['mensagem'] = 'Login realizado com sucesso!';
        $return['response']['classe'] = 'alert-success';
        $return['response']['result'] = 'success';
        $return['response']['redirect'] = '/sistema/';

        print_r(json_encode($return));
        exit();
    }

    public function retrievepassword($data = [])
    {

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $return['response']['mensagem'] = 'E-mail inválido!';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        }

        $admin = AccessUser::where('email', '=', $data['email'])->where('status', '=', 's')->first();
        $password = $this->gerar_password(8, true, true, true, false);

        $admin->password = md5($password);
        $admin->save();

        if (!isset($admin->email)) {
            $return['response']['mensagem'] = 'Usuário não encontrado!';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        }

        $email = [];
        $email[] = array('name' => $admin->name, 'email' => $admin->email);



        $mail = new Mail($this->datasite->emailuser, $this->datasite->emailpassword, $this->datasite->smtp, $this->datasite->port);
        $mail->fromname = 'Know How';
        $mail->recipient = $email;
        $mail->subject = addslashes(utf8_decode('Know How | Nova password!'));
        $mail->body = utf8_decode('
                      <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; padding-bottom:40px; border-collapse:collapse" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 24px; line-height: 1.5; text-align: center;">
                        Olá, <strong>' . $admin->name . '</strong>!</div>
                        </td>
                        </tr>
                        <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                        Sua nova password é <b>' . $password . '</b>
                        </div>
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                        <b>Dados de Acesso:</b><br><br>
                        <b>Usuário: ' . $admin->user . '</b><br>
                        <b>password: ' . $password . '</b><br>
                        </div>
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                        <a href="' . $this->url_default_site . '">Clique aqui</a> para acessar o sistema
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse;" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center; margin-top: 10px">

                        </div>
                        </td>
                        </tr>
                ');

        $mail->altbody = utf8_decode('Cebrom | Nova senha!');

        $send = $mail->send();

        if ($send) {
            $return['response']['mensagem'] = 'Senha enviada com sucesso! Verifique sua caixa de entrada!';
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['redirect'] = '';
        } else {
            $return['response']['mensagem'] = 'Não foi possivel enviar a password ao e-mail informado!';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
        }

        print_r(json_encode($return));
        exit();
    }

    public function logout()
    {
        session_start();
        session_destroy();
        $this->redirect('sistema.login');
    }
}
