<?php

namespace App\Controllers\Sistema;


use \App\Controllers\Sistema\Config;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessMenu;
use App\Models\Entities\Configuration;
use App\Models\Entities\SiteSeo;

/**
 * Description of Common
 *
 * @author oseas
 */
class Common extends Config
{

    public function logado()
    {
        $this->start_session();

        if (!isset($_SESSION['user_id'])) {
            $this->redirect('sistema.login');
        }

        $url = explode('/',  $_SERVER['REQUEST_URI']);
        $current = isset($url[2]) ? $url[2] : 'index';

        $current_url = isset($url[2]) ? explode('?', $url[2]) : [];

        if (count($current_url) > 0) {
            $atual = $current_url[0];
            $url[2] = $current_url[0];
        }

        $data['user'] = $_SESSION['user'];
        $data['password'] = $_SESSION['password'];

        $admin = AccessUser::where('user', '=', $data['user'])->where('password', '=', $data['password'])->where('status', '=', 'a')->first();

        if (!isset($admin->iduser)) {
            $this->redirect('sistema.login');
        }

        if (isset($url[2]) && $current == 'home' && $url[2] == 'perfil' && $url[3] <> $_SESSION['user_id']) {
            $this->redirect('sistema.login');
        }


        if ($current <> '' && $current <> 'home' && $current <> 'index' && $current <> 'dashboard' && $current <> 'perfil' && $current <> 'uploads' && $current <> 'data' && $current <> 'planning') {
            if (isset($admin->idgroup)) {
                $group = new AccessGroup();
                $menu = $group->checkpage($current, $admin->idgroup);
            }

            if (!isset($menu->idmenu) || $menu->idmenu == '') {
                $this->redirect('sistema.login');
            }
        }

        //$this->view->usertipo = $_SESSION['tipo'];
    }

    public function sidebar()
    {
        $this->start_session();

        $url = explode('/',  $_SERVER['REQUEST_URI']);
        $current = isset($url[2]) ? $url[2] : 'home';
        $group_access = new AccessGroup();
        $leveltop = $group_access->pagelist($_SESSION['idgroup'], 0);
        $menu = [];
        $submenu = [];
        $i = 0;

        foreach ($leveltop as $top) {
            $menu[$i]['title'] = $top->title;
            $menu[$i]['icon'] = $top->icon;
            $menu[$i]['level'] = $top->level;
            $menu[$i]['function'] = $top->function;
            $menu[$i]['idmenu'] = $top->idmenu;
            $menu[$i]['submenus'] = [];

            $submenus = $group_access->pagelist($_SESSION['idgroup'], $top->idmenu);

            $menu[$i]['total'] = count($submenus);

            $s = 0;
            foreach ($submenus as $sub) {
                $submenu[$s]['title'] = $sub->title;
                $submenu[$s]['icon'] = $sub->icon;
                $submenu[$s]['function'] = $sub->function;
                $s++;
            }
            $menu[$i]['submenus'] = $submenu;
            $i++;
        }

        $mretorno['leveltop'] = $menu;
        $mretorno['total'] = count($menu);
        $mretorno['current'] = $current;

        return $mretorno;
    }

    public function checkmenugroup($group, $menu)
    {
        $return = AccessMenu::join('access_groupxmenu', 'access_menu.idmenu', '=', 'access_groupxmenu.idmenu')
            ->where('access_groupxmenu.idgroup', '=', $group)->where('access_groupxmenu.idmenu', '=', $menu)
            ->select('access_groupxmenu.idmenu')->count();

        if ($return > 0) {
            $checked = 'checked="checked"';
        } else {
            $checked =  '';
        }

        return $checked;
    }

    public function info(){
        $this->datasite = Configuration::find(1);
        $this->seo = SiteSeo::find(1);
    }


}
