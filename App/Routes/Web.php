<?php

namespace App\Routes;

use \App\Core\Routes;

\Slim\Slim::registerAutoloader();

class Web extends Routes
{

    protected $routes = [];
    protected $routescrud = [];

    public function __construct()
    {
        $app = new \Slim\Slim(array(
            'templates.path' => 'templates'
        ));

        // rotas index
        $this->routes = [
            '/',
            '/home',
            '/index'
        ];

        foreach ($this->routes as $route) {
            $app->get($route, function () use ($app) {
                $this->initroute('Site', 'Home', 'index');
            });
        }

        $app->map('/blog', function () use ($app) {
            $this->initroute('Site', 'Blogs', 'index');
        })->via('GET', 'POST');

        $app->map('/videos', function () use ($app) {
            $this->initroute('Site', 'Videos', 'index');
        })->via('GET', 'POST');

        $app->map('/form-units', function () use ($app) {
            $this->initroute('Site', 'Home', 'formunits');
        })->via('GET', 'POST');

        $app->map('/contact', function () use ($app) {
            $this->initroute('Site', 'Home', 'contact');
        })->via('GET', 'POST');

        $app->map('/blog/:page', function ($page) use ($app) {
            $data[] = $page;
            $this->initroute('Site', 'Blogs', 'index', $data);
        })->via('GET', 'POST');

        $app->map('/blog/:text/:id', function ($text, $id) use ($app) {
            $data[] = $text;
            $data[] = $id;
            $this->initroute('Site', 'Blogs', 'post', $data);
        })->via('GET', 'POST');


        
        $app->run();
    }
}
