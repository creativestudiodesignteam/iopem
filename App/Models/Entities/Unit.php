<?php

namespace App\Models\Entities;

use App\Core\Models;
use \App\Models\Entities\Contacts;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Unit extends Models {
    protected $table = 'units';
    protected $primaryKey = 'idunits';
    public $timestamps = false;
    protected $fillable = [ 'title', 'address', 'email', 'phone', 'phone_add', 'image', 'status', 'text', 'type'];
    protected $guarded = [];

    public function contacts(){
        return $this->hasMany(Contacts::class, $this->primaryKey);
    }
}
