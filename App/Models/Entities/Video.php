<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\AccessUser;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Blog
 *
 * @author oseas
 */
class Video extends Models {
    protected $table = 'video';
    protected $primaryKey = 'idvideo';
    public $timestamps = false;
    protected $fillable = [ 'title', 'date_create', 'date_update', 'iduser', 'status', 'posting_date'];
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(AccessUser::class, 'iduser');
    }
}
