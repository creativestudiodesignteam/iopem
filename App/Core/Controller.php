<?php

namespace App\Core;

use MVC\Di\Container;
use App\Core\Action;

/**
 * Description of Controller
 *
 * @author oseas
 */
class Controller extends Action{

    protected $colors = array('-purple', '-green', '-yellow');
    protected $status = array(
      'a' => array('title' => 'Ativo', 'color' => 'badge-success'),
      'b' => array('title' => 'Bloqueado', 'color' => 'badge-danger'),
      'p' => array('title' => 'Pendente', 'color' => 'badge-warning'),
      'r' => array('title' => 'Recusado', 'color' => 'badge-danger'),
      'g' => array('title' => 'Resgatado', 'color' => 'badge-info')
    );

    public function model($model) {
        return Container::getClass($model);
    }

    public function rgb($hexdec){
        $hexdec = str_replace('#', '', $hexdec);
        $split = str_split($hexdec, 2);
        $r = hexdec($split[0]);
        $g = hexdec($split[1]);
        $b = hexdec($split[2]);

        return $r . ", " . $g . ", " . $b;
    }

    public function print_pre($input){
        echo '<pre>';
        print_r($input);
        exit();
    }

    public function porcentdiferenca($a, $b){
        if(floatval($b) == 0.00 || floatval($a) == 0.00){
            return 0.00;
        }

      return  ($b - $a) / $b * 100;
    }

    public function porcentdiferenca2($a, $b)
    {
        if (floatval($b) == 0.00 || floatval($a) == 0.00) {
            return 0.00;
        }

        return ($a / $b -1) * 100;
    }

    public function porcentdiferenca3($a, $b)
    {
        if (floatval($b) == 0.00 || floatval($a) == 0.00) {
            return 0.00;
        }


        return ($b - $a) / $b;
    }

    public function porcentagem_nx($parcial, $total)
    {
        return $parcial > 0 && $total > 0 ? ($parcial * 100) / $total : 0;
    }

    public function daymonthlabel($t, $i){
      $array = [];
      $array['m'] = array(
          1 => 'Janeiro',
          'Fevereiro',
          'Março',
          'Abril',
          'Maio',
          'Junho',
          'Julho',
          'Agosto',
          'Setembro',
          'Outubro',
          'Novembro',
          'Dezembro'
      );
      $array['d'] = array(
          1 => 'Domingo',
          'Segunda-Feira',
          'Terça-Feira',
          'Quarta-Feira',
          'Quinta-Feira',
          'Sexta-Feira',
          'Sábado'
      );

      return $array[$t][$i];
    }

    public function returnmonth($m){
        $array = array(
          1 => 'Janeiro',
          'Fevereiro',
          'Março',
          'Abril',
          'Maio',
          'Junho',
          'Julho',
          'Agosto',
          'Setembro',
          'Outubro',
          'Novembro',
          'Dezembro'
        );

        return $array[$m];

    }

    public function dateinterval($start, $end){
      $datatime1 = new \DateTime($start);
      $datatime2 = new \DateTime($end);

      $data1  = $datatime1->format('Y-m-d H:i:s');
      $data2  = $datatime2->format('Y-m-d H:i:s');

      $diff = $datatime1->diff($datatime2);
      $horas = $diff->h + ($diff->days * 24);

      return $horas;

    }

    public function generate_string($strength = 6) {
        $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }

    public function searchDay($d){
      $phrase  = $d;
      $healthy = [1, 2, 3, 4, 5, 6, 7, 8];
      $yummy   = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Fer'];

      $newPhrase = str_replace($healthy, $yummy, $phrase);

      return $newPhrase;
    }

    public function limita_caracteres($texto, $limite, $quebra = true){
       $tamanho = strlen($texto);
       if($tamanho <= $limite){ //Verifica se o tamanho do texto é menor ou igual ao limite
          $novo_texto = $texto;
       }else{ // Se o tamanho do texto for maior que o limite
          if($quebra == true){ // Verifica a opção de quebrar o texto
             $novo_texto = trim(substr($texto, 0, $limite))."...";
          }else{ // Se não, corta $texto na última palavra antes do limite
             $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
             $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."..."; // Corta o $texto até a posição localizada
          }
       }
       return $novo_texto; // Retorna o valor formatado
    }

    public function yearold18(){
        $atual = date('d-m-Y');
        return date('Y-m-d', strtotime('-6570 days', strtotime($atual)));
    }

    public function antiInjection ($var,$q='') {
        //Verifica se o parâmetro é um array
        if (!is_array($var)) {
            //identifico o tipo da variável e trato a string
            switch (gettype($var)) {
                case 'double':
                case 'integer':
                    if($var==null) {
                        $var = 0;
                    }
                    $return = $var;
                    break;
                case 'string':
                    /*Verifico quantas vírgulas tem na string.
                      Se for mais de uma trato como string normal,
                      caso contrário trato como String Numérica*/
                    $temp = (substr_count($var,',')==1) ? str_replace(',','*;*',$var) : $var;
                    //aqui eu verifico se existe valor para não adicionar aspas desnecessariamente
                    if (!empty($temp)) {
                        if (is_numeric(str_replace('*;*','.',$temp))) {
                            $temp = str_replace('*;*','.',$temp);
                            $return = strstr($temp,'.') ? floatval($temp) : intval($temp);
                        } elseif (get_magic_quotes_gpc()) {
                            //aqui eu verifico o parametro q para o caso de ser necessário utilizar LIKE com %
                            $return = (empty($q)) ? '\''.str_replace('*;*',',',$temp).'\'' : '\'%'.str_replace('*;*',',',$temp).'%\'';
                        } else {
                            //aqui eu verifico o parametro q para o caso de ser necessário utilizar LIKE com %
                            $return = (empty($q)) ? '\''.addslashes(str_replace('*;*',',',$temp)).'\'' : '\'%'.addslashes(str_replace('*;*',',',$temp)).'%\'';
                        }
                    } else {
                        // $return = $temp;
                        return '\'\'';
                    }
                    break;
                case '':
                    return '\'\'';
                    break;
                case empty($temp):
                    return '\'\'';
                    break;
                case null:
                    return '\'\'';
                    break;
                default:
                    /*Abaixo eu coloquei uma msg de erro para poder tratar
                      antes de realizar a query caso seja enviado um valor
                      que nao condiz com nenhum dos tipos tratatos desta
                      função. Porém você pode usar o retorno como preferir*/
                    $return = '\'\'';
            }
            //Retorna o valor tipado
            return $return;
        } else {
            //Retorna os valores tipados de um array
            return array_map('antiInjection',$var);
        }
    }

    public function getToday(){
        return date('Y-m-d H:i:s');
    }

    public function limits_characters($texto, $limite, $quebra = true){
        $tamanho = strlen($texto);
        if($tamanho <= $limite){ //Verifica se o tamanho do texto é menor ou igual ao limite
            $novo_texto = $texto;
        }else{ // Se o tamanho do texto for maior que o limite
            if($quebra == true){ // Verifica a opção de quebrar o texto
                $novo_texto = trim(substr($texto, 0, $limite))."...";
            }else{ // Se não, corta $texto na última palavra antes do limite
                $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
                $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."..."; // Corta o $texto até a posição localizada
            }
        }
        return $novo_texto; // Retorna o valor formatado
    }

    public function url_generate($string)
    {
        $string = trim($string);
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Ð'=>'D', 'd'=>'d', 'Ž'=>'Z',
            'ž'=>'z', 'C'=>'C', 'c'=>'c', 'C'=>'C', 'c'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'R'=>'R', 'r'=>'r',
        );
        // Traduz os caracteres em $string, baseado no vetor $table
        $string = strtr($string, $table);
        // converte para minúsculo
        $string = strtolower($string);
        // remove caracteres indesejáveis (que não estão no padrão)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        // Remove múltiplas ocorrências de hífens ou espaços
        $string = preg_replace("/[\s-]+/", " ", $string);
        // Transforma espaços e underscores em hífens
        $string = preg_replace("/[\s_]/", "-", $string);
        // retorna a string
        return $string;
    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    // Pega o SESSION_ID
    function get_sessao(){
        if(empty(session_id())){
            session_start();
        }
        return session_id();
    }

    /*CRIA UM COOKIE*/
    function set_cookie($name, $value){
        return setcookie($name, $value,time()+112);
    }

    public function which_device(){
        $mobile = FALSE;

        $user_agents = array("iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","IsGeneric");

        foreach($user_agents as $user_agent){
            if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE) {
                $mobile = TRUE;
                $modelo = $user_agent;
                break;
            }
        }

        if($mobile){
            return $modelo;
        }else{
            return "Computador";
        }
    }

    public function tagreplace($field){
        $val = trim(strip_tags($field));
        $val = preg_replace('[\r\n]', '\n', $val);
        $val = preg_replace('[&quot;]', '"', $val);
        $val = preg_replace('[&amp;]', '&', $val);
        $val = preg_replace('/(\v|\s)+/', ' ', $val);
        $val = htmlspecialchars(trim(strip_tags($val)));

        return $val;
    }

    public function gerar_senha($tamanho, $maiusculas, $minusculas, $numeros, $simbolos){
        $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiúsculas
        $mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
        $nu = "0123456789"; // $nu contem os números
        $si = "!@#$%¨&*()_+="; // $si contem os símbolos

        $senha = '';
        if ($maiusculas){
            // se $maiusculas for "true", a variável $ma é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($ma);
        }

        if ($minusculas){
            // se $minusculas for "true", a variável $mi é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($mi);
        }

        if ($numeros){
            // se $numeros for "true", a variável $nu é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($nu);
        }

        if ($simbolos){
            // se $simbolos for "true", a variável $si é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($si);
        }

        // retorna a senha embaralhada com "str_shuffle" com o tamanho definido pela variável $tamanho
        return substr(str_shuffle($senha),0,$tamanho);
    }

    public function upload($file, $path)
    {
        ini_set('upload_max_filesize', '20M');
        // A list of permitted file extensions
        $allowed = array('png', 'jpg', 'gif', 'jpeg', 'pdf', 'doc', 'zip', 'rar', 'xlsx', 'csv');
        $this->start_session();
        $response = [];
        $new_name = '';

        if(isset($file['file']) && $file['file']['error'] == 0){
            $caminho = $_SERVER['DOCUMENT_ROOT'].'/images/'.$path;
            $c = 'images';

            $extension = pathinfo($file['file']['name'], PATHINFO_EXTENSION);
            //echo $extension;

            if(!in_array(strtolower($extension), $allowed)){
                $response['success'] = false;
                $response['message'] = 'As extensões permitidas são: '.implode(',', $allowed);
            }

            if( $extension == 'pdf' || $extension == 'doc' || $extension == 'zip' || $extension == 'rar' || $extension == 'xlsx' || $extension == 'csv'){
                $caminho = str_replace('images', 'files', $caminho);
                $c = str_replace('images', 'files', $c);;
            }

            if (!is_dir($caminho)) {
                mkdir($caminho, 0777);
            }

            $new_name = md5($file['file']['name'].date('YmdHis')).'.'.$extension;

            if(move_uploaded_file($file['file']['tmp_name'], $caminho.'/'.$new_name)){
                $response['success'] = true;
                $response['message'] = 'Upload feito com sucesso!';
                $response['image'] = '/'. $c. '/'.$path.'/'.$new_name;
            }else{
              $response['success'] = false;
              $response['message'] = 'Ocorreu algum erro ao subir a imagem!';
            }
        } else {
            $response['success'] = false;
            $response['message'] = 'As extensões permitidas são: ' . implode(',', $allowed);
        }


        return $response;
    }

    public function gerar_hash($password){
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);

        return $encrypted;
    }


    public function idade($date){
      // Separa em dia, mês e ano
      list($ano, $mes, $dia) = explode('-', $date);

      // Descobre que dia é hoje e retorna a unix timestamp
      $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
      // Descobre a unix timestamp da data de nascimento do fulano
      $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

      // Depois apenas fazemos o cálculo já citado :)
      $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

      return $idade;
    }

    public function start_session(){
        if(empty(session_id())){
            session_start();
        }
    }
}
