<?php

namespace App\Core;

use \App\Routes\Intranet;
use \App\Routes\Web;
use \App\Routes\Client;
use \App\Routes\Totem;

class App {
    /**
     * App constructor.
     */
    public function __construct() {
        $dominio = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = (explode('/', $dominio));

        if($url[1] == 'sistema'){
            $intranet = new Intranet();
        }else if($url[1] == 'areadocliente'){
            $client = new Client();
        }else if($url[1] == 'totem'){
            $client = new Totem();
        }else{
            $web = new Web();
        }

    }

}
